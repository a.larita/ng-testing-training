// import angular from 'angular';

describe('Users factory', () => {
    let Users;

    let UserLists = [
        {id:1, name: 'Assh', role: 'Junior Web Developer'},
        {id:2, name: 'Jiriko', role: 'Senior Web Developer'},
        {id:3, name: 'Donald', role: 'Fullstack Senior Jedi All Around Developer'},
        {id:5, name: 'Jericho Lapa', role: 'Idol!!'},
    ]

    beforeEach(angular.mock.module('api.users'));

    beforeEach(inject(_Users_ => {
        Users = _Users_;
    }));

    it('should exists', () => {
        expect(Users).toBeDefined();
    });

    // A set of test for Users.all()
    describe('.all()', () => {
        it('should exists', () => {
            expect(Users.all).toBeDefined();
        });

        it('it should return a list of users', () => {
            expect(Users.all()).toEqual(UserLists);
        });
    });

    // A set of tests for Users.findById()
    describe('.findById()', () => {
        it('should exists', () => {
            expect(Users.findById).toBeDefined();
        });

        it('returns one user object if exists', () => {
            expect(Users.findById(2)).toEqual(UserLists[1]);
        });

        it('should return undefined if the user does not exist', () => {
            expect(Users.findById(999)).not.toBeDefined();
        });
    });
});
