(app => {
    'use strict';

    app = angular.module('api.users', []);

    app.factory('Users', () => {
        return {
            /**
             * Collections of Users Object
             *
             * @return {Array}
             */
            all: () => [
                {id:1, name: 'Assh', role: 'Junior Web Developer'},
                {id:2, name: 'Jiriko', role: 'Senior Web Developer'},
                {id:3, name: 'Donald', role: 'Fullstack Senior Jedi All Around Developer'},
                {id:5, name: 'Jericho Lapa', role: 'Idol!!'},
            ],

            /**
             * Find User by its ID
             *
             * @param  {integer} id
             * @return {Object}
             */
            findById (id) {
                return this.all().find(user => user.id === id);
            }
        };
    });

})();
