(app => {
    'use strict';

    app = angular.module('components.users', []);

    app.controller('UsersController', function (Users) {
        this.users = Users.all();
    });

    app.config($stateProvider => {
        // Define states
        let states = [
            {
                name: 'users',
                url: '/users',
                templateUrl: 'components/users/users.html',
                controller: 'UsersController as $this'
            }
        ];

        // Register states for UserController
        states.map(state => $stateProvider.state(state.name, state));
    });

})();
