describe('UsersController', () => {
    let $controller, UsersController, UsersFactory;

    // Mocking the lists...
    let UserLists = [
        {id:1, name: 'Assh', role: 'Junior Web Developer'},
        {id:2, name: 'Jiriko', role: 'Senior Web Developer'},
        {id:3, name: 'Donald', role: 'Fullstack Senior Jedi All Around Developer'},
        {id:10, name: 'Jericho Lapa', role: 'Idol!!'},
    ];

    let modules = ['ui.router', 'components.users', 'api.users'].map(
        module => beforeEach( angular.mock.module(module) )
    );

    // beforeEach(angular.mock.module('ui.router'));
    // beforeEach(angular.mock.module('components.users'));
    // beforeEach(angular.mock.module('api.users'));

    beforeEach(inject((_$controller_, _Users_) => {
        $controller = _$controller_;
        UsersFactory = _Users_;

        // Spy Users.all() factory when called and force return the mock list of Users.
        spyOn(UsersFactory, 'all').and.callFake(() => UserLists);

        UsersController = $controller('UsersController');
    }));

    it('should be defined', () => {
        expect(UsersController).toBeDefined();
    });

    it('should initialized with a call to Users.all()', () => {
        expect(UsersFactory.all).toHaveBeenCalled();
        expect(UsersController.users).toEqual(UserLists);
    });
});
