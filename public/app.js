(app => {
    'use strict';

    app = angular.module('app', ['ui.router', 'api.users', 'components.users']);

    app.config($urlRouterProvider => $urlRouterProvider.otherwise('/users'));

})()
